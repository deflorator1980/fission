from minio import Minio

def main():
    mc = Minio('10.10.4.197:9000', access_key='postgres', secret_key='postgres', secure=False)
    buckets = mc.list_buckets()
    return buckets[0].name 

# if __name__ == '__main__':
#     print(main())    