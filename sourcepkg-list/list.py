import sh
import subprocess

def main():
    # return str(sh.pwd())
    # return str(subprocess.check_output(['ls', '-la'], stderr=subprocess.STDOUT))
    return str(subprocess.check_output(['ls'], stderr=subprocess.STDOUT))

if __name__ == '__main__':
    print(main())    