from minio import Minio
import subprocess
import postgresql
from flask import request

def main():
    # FILE_NAME='ala.jpg'
    FILE_NAME = request.args.get('file')
    mc = Minio('10.10.4.197:9000', access_key='postgres', secret_key='postgres', secure=False)
    mc.fget_object('foo', FILE_NAME, FILE_NAME)

    db = postgresql.open('pq://postgres:postgres@10.10.4.197:5432')
    fin = open(FILE_NAME, 'rb') 
    img = fin.read()
    ins = db.prepare("insert into images (imgname, img) values ($1, $2)")
    ins(FILE_NAME,img) 

    # return str(subprocess.check_output(['ls'], stderr=subprocess.STDOUT)).replace("\\n", " ") 
    return str(db.query('select imgname from images')) 

# if __name__ == '__main__':
#     print(main())    
