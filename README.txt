CREATE TABLE images (imgname text, img bytea);

fission env create --name python --image fission/python-env --builder fission/python-builder
minikube -n fission service router --url
export FISSION_ROUTER=172.17.0.2:31314

fis fn delete --name get-put
fis pkg delete --name get-put-zip-jcpt
zip -jr get-put.zip get-put/
fis pkg create --sourcearchive get-put.zip --env python --buildcmd "./build.sh"
fis pkg info --name get-put-zip-kvta
fis fn create --name get-put --pkg get-put-zip-kvta --entrypoint "get_put.main"
# fis fn test --name get-put

fission route delete --function get-put
fis route create --method GET --url /get-put --function get-put
curl $FISSION_ROUTER/get-put?file=chou.jpg